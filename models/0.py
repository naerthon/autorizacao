from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'AUTORIZACAO'
settings.subtitle = ''
settings.author = 'NAERTHON'
settings.author_email = 'naerthon@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'naerthon'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '9dcfab62-d130-477e-a462-95950c4a4b08'
settings.email_server = 'localhost'
settings.email_sender = 'naerthon@gmail.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []

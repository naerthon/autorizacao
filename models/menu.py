response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.menu = [
(T('Index'),False,URL('default','index'),[]),
(T('Autorizações'),False,URL('autorizacoes','index'),[]),
(T('Secretarias'),False,URL('secretarias','index'),[]),
(T('Anos'),False,URL('autorizacoes','anos'),[]),
]

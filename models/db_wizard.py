db.define_table('estados',
		Field('estado', type='string'),
	format='%(estado)s'
	)


db.define_table('cidades',
	Field('cidade',type='string',
		label=T('Cidade')),
	Field('estados','reference estados',
		label=T('Estado')),
	format='%(cidade)s'
	)

db.define_table('secretarias',
    Field('secretaria', type='string',
          label=T('Secretaria')),
    format='%(secretaria)s')


db.define_table('contratados',
    Field('nome', type='string',
          label=T('Nome')),
    Field('codigo', type='integer',
          label=T('Código')),
    Field('cadastro', type='integer',
          label=T('Cadastro')),
    Field('conta', type='string',
          label=T('Conta')),
    Field('banco_name', type='string',
          label=T('Banco')),
    Field('codigo', type='integer',
          label=T('Código')),
    Field('endereco', type='string',
          label=T('Endereço')),
    Field('cep', type='integer',
          label=T('CEP')),
    Field('cidade', 'reference cidades'),
    Field('CNPJ', type='integer',
          label=T('CNPJ')),
    Field('inscricao', type='integer',
          label=T('Inscrição')),
    format='%(nome)s'
    )

db.define_table('licitacoes',
	Field('licitacao', type='string'
		),
	format='%(licitacao)s'
	)

anos=db.define_table('anos',
	Field('ano', type='integer', label='Ano'),
	format='%(ano)s',
	)


autorizacoes=db.define_table('autorizacoes',
	Field('codigo', type='integer',
		label='Cod'),
	Field('ano', 'reference anos'),
	Field('secretaria', 'reference secretarias'),
	Field('contratado', 'reference contratados'),
	Field('pa', type='integer',
		label='Projeto/Atividade'),
	Field('ed', type='integer',
		label='Elemento de Despesa'),
	Field('fr', type='integer',
		label='Fonte de Recurso'),
	Field('valor', type='integer'),
	Field('parcelas', type='integer'),
	Field('contrato', type='string'),
	Field('licitacao', 'reference licitacoes'),
	Field('n_modalidade', type='string'),
	Field('itens', type='list:string'),
	Field('quantidade', type='list:string'),
	Field('unidade', type='list:string'),
	)



def get_header_labels(table=None):
    headers = {}
    for field in db[table].fields:
        headers[table+'.'+field] = db[table][field].label
    return headers